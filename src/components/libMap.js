function addAttrSvgFromBounds(bounds, svg) {
  const topLeft = bounds[0];
  const bottomRight = bounds[1];
  svg
    .attr('width', bottomRight[0] - topLeft[0])
    .attr('height', bottomRight[1] - topLeft[1])
    .style('left', `${topLeft[0]}px`)
    .style('top', `${topLeft[1]}px`);
}

function getRandomColor() {
  const letters = '0123456789ABCDE';
  let color = '#';
  for (let i = 0; i < 6; i += 1) {
    color += letters[Math.floor(Math.random() * 15)];
  }
  return color;
}

module.exports = { addAttrSvgFromBounds, getRandomColor };
