import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Svgs from '@/components/Svgs';
import MapBase from '@/components/MapBase';
import MapProvinces from '@/components/MapProvinces';
import SvgRotation from '@/components/svgRotation';
import MapLeaflet from '@/components/MapLeaflet';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/svgs',
      name: 'Svgs',
      component: Svgs,
    },
    {
      path: '/mapbase',
      name: 'MapBase',
      component: MapBase,
    },
    {
      path: '/mapprovinces',
      name: 'MapProvinces',
      component: MapProvinces,
    },
    {
      path: '/svgrotation',
      name: 'SvgRotation',
      component: SvgRotation,
    },
    {
      path: '/mapleaflet',
      name: 'MapLeaflet',
      component: MapLeaflet,
    },
  ],
});
